## Townsfolk Tracker v1

[See the latest version's changes here.](https://bitbucket.org/jsiebert9/townsfolk-tracker/src/master/changelog.md)

### v1.0.6 (Sept. 24, 2019)
* Added a slash command to open the addon's interface options: /townsfolktracker, /townsfolk, /tt
* Tentatively fixed the taint issue with various unit frames (CompactRaidFrame, etc.).
* Fixed display conflicts of the Map button with other addons.
* Updated Chinese localization up to version 1.0.5; translations by [急云](https://www.curseforge.com/members/q09q09)
* Added Traditional Chinese localization up to version 1.0.5, not including NPC names; translations by [急云](https://www.curseforge.com/members/q09q09)


### v1.0.5 (Sept. 21, 2019)
* Added Swamp of Sorrows NPCs
* Added The Hinterlands NPCs
* Added Western Plaguelands NPCs
* Added Blasted Lands NPCs
* Added Stonetalon Peak, Stonetalon Mountains NPCs
* Added Brackenwall Village, Dustwallow Marsh NPCs
* Added Bloodvenom Post, Felwood NPCs
* Added a few missing NPCs in:
  * Thunderbluff
  * Desolace (Horde)
  * Tirisfal Glades
  * Sun Rock Retreat, Stonetalon Mountains
* Fixed the doubled mailbox in Theramore Isle
* Updated Chinese localization up to version 1.0.4; translations by [急云](https://www.curseforge.com/members/q09q09)

### v1.0.4 (Sept. 17, 2019)
* Added Alterac Mountains NPCs
* Added Alliance Hillsbrad Foothills NPCs
* Added Alliance Arathi Highlands NPCs
* Added Shadowprey Village, Desolace NPCs (Horde)
* Added remaining Azshara NPCs (Horde, neutrals)
* Added a few missing Elwynn Forest NPCs
* Added Chinese localization up to version 1.0.3; translations by [急云](https://www.curseforge.com/members/q09q09)

### v1.0.3 (Sept. 15, 2019)
* Added Darkshore NPCs
* Added Redridge Mountains NPCs
* Added Duskwood NPCs
* Added Alliance Ashenvale NPCs
* Added Alliance Azshara NPCs
* Added Theramore Isle, Dustwallow Marsh NPCs
* Added Alliance Arathi Highlands NPCs
* Added several missing NPCs to Stranglethorn Vale
* Added several missing NPCs to Wetlands
* Minor NPC fixes in Stormwind, Darnassus

### v1.0.2 (Sept. 13, 2019)
* Added Thousand Needles NPCs
* Added Camp Mojache, Feralas NPCs
* Added Tanaris NPCs
* Added Elwynn Forest NPCs
* Added Westfall NPCs
* Added a few missing Stormwind NPCs
* Minor locale fixes

### v1.0.1 (Sept. 12, 2019)
* Fixes to enable standalone usage
* Curse packaging modifications

### v1.0 (Sept. 11, 2019)
* Initial release
