### v1.0.7 (Oct. 12, 2019)
* Fixed the SoundKit issue with the dropdown menu.
* Added a few missing NPCs to Orgrimmar and Silverpine Forest.

[See past changes here.](https://bitbucket.org/jsiebert9/townsfolk-tracker/src/master/changehistory.md)
